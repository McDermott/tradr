import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LandingPageComponent } from '../_pages/landing-page/landing-page.component';
import { RegistrationPageComponent } from '../_pages/registration-page/registration-page.component';
import { RegistrationPageResolver } from '../_pages/registration-page/registration-page.resolver';
import { LandingPageResolver } from '../_pages/landing-page/landing-page.resolver';
import { LoginPageComponent } from '../_pages/login-page/login-page.component';
import { LoginPageResolver } from '../_pages/login-page/login-page.resolver';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  {
    path: 'home',
    component: LandingPageComponent,
    resolve: { message: LandingPageResolver },
  },

  {
    path: 'register',
    component: RegistrationPageComponent,
    resolve: { message: RegistrationPageResolver },
  },
  {
    path: 'login',
    component: LoginPageComponent,
    resolve: { message: LoginPageResolver },
  },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [RegistrationPageResolver, LoginPageResolver, LandingPageResolver],
})
export class AppRoutingModule {}
