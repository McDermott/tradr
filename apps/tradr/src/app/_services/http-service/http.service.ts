import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class HttpService {
  private readonly api = environment.API;

  constructor(private readonly _httpClient: HttpClient) {}

  login(body: { username: string; password: string }): Observable<any> {
    return this._httpClient.post(environment.API + '/auth/login', body);
  }

  getUser(authString: string): Observable<any> {
    const headers = {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + authString,
    };
    return this._httpClient.get(environment.API + '/profile', { headers });
  }
}
