import { Injectable } from '@angular/core';
import { HttpService } from '../http-service/http.service';
import {Observable, Subject} from 'rxjs';
import { environment } from '../../../environments/environment';
import { Router } from '@angular/router';
import { ILoggedInUser } from '../../../../../../libs/api-interfaces/src/lib/user.interface';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  public readonly loggedInUser: Subject<ILoggedInUser | undefined> = new Subject<ILoggedInUser | undefined>()

  constructor(
    private readonly _httpService: HttpService,
    private readonly _router: Router
  ) {}

  setUserTokenInStorage(userAuthToken: string): void {
    localStorage.setItem(environment.AUTH_TOKEN_ID, userAuthToken);
  }

  getUserTokenInStorage(): string | undefined | null {
    if (localStorage.getItem(environment.AUTH_TOKEN_ID)) {
      return localStorage.getItem(environment.AUTH_TOKEN_ID);
    }
    return undefined;
  }

  async userLoggedIn(): Promise<void> {
    if (!localStorage.getItem(environment.AUTH_TOKEN_ID)) {
      await this._router.navigate(['login']);
    } else {
      await this._httpService
        .getUser(this.getUserTokenInStorage() || '')
        .subscribe(
          (user: ILoggedInUser) => {
            this.loggedInUser.next(user);
          },
          () => {
            this.loggedInUser.next(undefined);
            this.logOut();
          }
        );
    }
  }

  async logOut(): Promise<void> {
    localStorage.removeItem(environment.AUTH_TOKEN_ID);
    await this.userLoggedIn();
  }

  login(body: { username: string; password: string }): Observable<any> {
    return this._httpService.login(body);
  }
}
