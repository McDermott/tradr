import { Injectable } from '@angular/core';
import {
  Router,
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot,
} from '@angular/router';
import { Observable, of } from 'rxjs';
import {UserService} from "../../_services/user-service/user.service";

@Injectable({
  providedIn: 'root',
})
export class LandingPageResolver implements Resolve<boolean> {

  constructor(private readonly _userService: UserService) {
  }
  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> {

    this._userService.userLoggedIn();

    return of(true);
  }
}
