import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LandingPageComponent } from './landing-page.component';
import { RouterLinkWithHref } from '@angular/router';
import {ReuseableModule} from "../../_components/_reuseable/reuseable.module";

@NgModule({
  declarations: [LandingPageComponent],
    imports: [CommonModule, RouterLinkWithHref, ReuseableModule],
})
export class LandingPageModule {}
