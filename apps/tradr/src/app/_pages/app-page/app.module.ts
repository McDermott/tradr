import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';

import { HttpClientModule } from '@angular/common/http';
import { LandingPageModule } from '../landing-page/landing-page.module';
import { RegistrationPageModule } from '../registration-page/registration-page.module';
import { LoginPageModule } from '../login-page/login-page.module';
import { AppRoutingModule } from '../../router/router.module';
import { ReuseableModule } from '../../_components/_reuseable/reuseable.module';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    HttpClientModule,
    LandingPageModule,
    RegistrationPageModule,
    LoginPageModule,
    AppRoutingModule,
    ReuseableModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
