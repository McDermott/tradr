import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegistrationPageComponent } from './registration-page.component';
import { RouterLinkWithHref } from '@angular/router';
import {ReuseableModule} from "../../_components/_reuseable/reuseable.module";

@NgModule({
  declarations: [RegistrationPageComponent],
  imports: [CommonModule, RouterLinkWithHref, ReuseableModule],
})
export class RegistrationPageModule {}
