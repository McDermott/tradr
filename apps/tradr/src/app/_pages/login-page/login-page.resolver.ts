import { Injectable } from '@angular/core';
import {
  Router,
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot,
} from '@angular/router';
import { Observable, of, tap } from 'rxjs';
import { UserService } from '../../_services/user-service/user.service';

export interface ILoginPage {
  login: (body: { username: string; password: string }) => void;
}

@Injectable({
  providedIn: 'root',
})
export class LoginPageResolver implements Resolve<ILoginPage> {
  constructor(
    private readonly _userService: UserService,
    private readonly _router: Router
  ) {}
  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<ILoginPage> {
    return of({
      login: (body: { username: string; password: string }) => {
        return this._userService.login(body).pipe(
          tap(async (val) => {
            if (val.access_token) {
              this._userService.setUserTokenInStorage(val.access_token);
              if (this._userService.getUserTokenInStorage()) {
                await this._userService.userLoggedIn();
                await this._router.navigate(['home']);
              }
            }
          })
        );
      },
    });
  }
}
