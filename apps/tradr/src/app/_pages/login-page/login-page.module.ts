import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginPageComponent } from './login-page.component';
import { RouterLinkWithHref } from '@angular/router';
import { ReuseableModule } from '../../_components/_reuseable/reuseable.module';

@NgModule({
  declarations: [LoginPageComponent],
  imports: [CommonModule, RouterLinkWithHref, ReuseableModule],
})
export class LoginPageModule {}
