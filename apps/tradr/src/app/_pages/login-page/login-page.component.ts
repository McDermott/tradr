import { Component, OnInit } from '@angular/core';
import { ILoginPage } from './login-page.resolver';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'tradr-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss'],
})
export class LoginPageComponent implements OnInit {
  readonly pageData = this._activatedRoute.snapshot.data['message'];

  userName?: string;
  password?: string;

  loginError = false;
  constructor(private readonly _activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {}

  onSubmit(): void {
    this.pageData
      .login({ username: this.userName, password: this.password })
      .subscribe(
        () => {},
        (err: never) => {
          console.log(err);
          if (err) {
            this.loginError = true;
          }
        }
      );
  }
}
