import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'tradr-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss'],
})
export class InputComponent {
  @Input() readonly = false;
  @Input() placeholder = '';
  @Input() inputType?: string;
  @Input() hasError = false;
  @Input() label?: string;

  @Output() output: EventEmitter<string> = new EventEmitter<string>();

  emitInput(event: any) {
    this.output.emit(event.target.value);
  }

  setInputType() {
    if (this.inputType) {
      return this.inputType;
    }
    return 'text';
  }
}
