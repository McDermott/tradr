import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'tradr-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
})
export class ButtonComponent {
  @Input() label: string = 'Submit';

  @Output() output: EventEmitter<any> = new EventEmitter<any>();
}
