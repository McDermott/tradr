import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputComponent } from '../input/input.component';
import { ToggleComponent } from '../toggle/toggle.component';
import { SelectComponent } from '../select/select.component';
import { ButtonComponent } from '../button/button.component';
import {HeaderComponent} from "../header/header.component";
import {RouterLink} from "@angular/router";

@NgModule({
  declarations: [
    InputComponent,
    ToggleComponent,
    SelectComponent,
    ButtonComponent,
    HeaderComponent
  ],
  exports: [InputComponent, ToggleComponent, SelectComponent, ButtonComponent, HeaderComponent],
  imports: [CommonModule, RouterLink],
})
export class ReuseableModule {}
