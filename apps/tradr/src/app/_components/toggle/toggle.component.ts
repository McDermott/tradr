import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'tradr-toggle',
  templateUrl: './toggle.component.html',
  styleUrls: ['./toggle.component.scss'],
})
export class ToggleComponent {
  @Input() checked = false;
  @Output() toggle: EventEmitter<boolean> = new EventEmitter<boolean>();

  toggleCheck() {
    this.checked = !this.checked;
    this.toggle.emit(this.checked);
  }
}
