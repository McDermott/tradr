import { Component, OnInit } from '@angular/core';
import {UserService} from "../../_services/user-service/user.service";
import {ILoggedInUser} from "../../../../../../libs/api-interfaces/src/lib/user.interface";

@Component({
  selector: 'tradr-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {

  loggedInUser?: ILoggedInUser;
  constructor(private readonly _userService: UserService) {}

  ngOnInit(): void {
    this._userService.loggedInUser.subscribe(user => {
      this.loggedInUser = user;
    })
  }

  async logOut() {
    await this._userService.logOut();
  }
}
