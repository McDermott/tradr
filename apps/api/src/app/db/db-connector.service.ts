import { Injectable, Inject } from '@nestjs/common';
import { MongoClient } from 'mongodb';

@Injectable()
export class DbConnectorService {
  private databaseConnector: MongoClient = new MongoClient(this.dbUri);
  private dataBase = this.databaseConnector.db('tradr');
  private userVault = this.dataBase.collection('user_vault');
  private storeVault = this.dataBase.collection('store_vault');

  constructor(@Inject('URI_STRING') private dbUri: string) {}

  public getStoreVault() {
    this.databaseConnector.connect();
    return this.storeVault;
  }

  public getUserVault() {
    this.databaseConnector.connect();
    return this.userVault;
  }

  public async closeConnection(): Promise<void> {
    await this.databaseConnector.close();
  }
}
