import { Injectable } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
import { IUserInterface } from '../../../../../libs/api-interfaces/src/lib/user.interface';

const bcrypt = require('bcrypt');

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService
  ) {}

  async validateUser(username: string, pass: string): Promise<any> {
    const user: IUserInterface = await this.usersService.findOne(username);

    if (user) {
      const isPasswordMatching = await bcrypt.compare(pass, user.password);
      if (isPasswordMatching) {
        const { password, ...result } = user;
        return result;
      }
      return null;
    }
    return null;
  }

  async login(user: any) {
    const payload = {
      username: user.username,
      first_name: user.first_name,
      last_name: user.last_name,
      email: user.email,
      public_guid: user.public_guid,
      store_data_guid: user.store_data_guid,
    };
    return {
      access_token: this.jwtService.sign(payload),
    };
  }
}
