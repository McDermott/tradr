import { Module } from '@nestjs/common';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DbModule } from './db/db.module';
import { SharedModule } from './shared/shared.module';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { StoreModule } from './store/store.module';
import { SearchModule } from './search/search.module';
import { environment } from '../environments/environment';

@Module({
  imports: [
    DbModule.forRoot(environment.DATABASE_URL),
    AuthModule,
    UsersModule,
    SharedModule,
    StoreModule,
    SearchModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
