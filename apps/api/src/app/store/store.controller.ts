import { Controller, Get, Post, Request, Res } from '@nestjs/common';
import {
  IGenericMessage,
  IStoreInterface,
} from '../../../../../libs/api-interfaces/src/lib/user.interface';
import { StoreService } from './store.service';

@Controller()
export class StoreController {
  constructor(private _storeService: StoreService) {}

  @Get('stores')
  async getStores(): Promise<Array<IStoreInterface>> {
    return await this._storeService.findAllStores();
  }

  @Post('get-store')
  async getSingleStore(@Request() req): Promise<Array<IStoreInterface>> {
    return await this._storeService.findSingleStore(req.body.public_guid);
  }

  @Post('message-store')
  async postMessageToStore(@Request() req): Promise<IGenericMessage | number> {
    return await this._storeService.postMessageToStore(req.body);
  }

  @Post('get-store-messages')
  async getStoreMessages(@Request() req): Promise<IGenericMessage | number> {
    return await this._storeService.getStoreMessages(req.body);
  }
}
