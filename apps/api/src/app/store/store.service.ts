import { Injectable } from '@nestjs/common';
import { DbConnectorService } from '../db/db-connector.service';
import {
  IStoreInterface,
  IGenericMessage,
} from '../../../../../libs/api-interfaces/src/lib/user.interface';
import { environment } from '../../environments/environment';

@Injectable()
export class StoreService {
  constructor(private readonly _dbConnectorService: DbConnectorService) {}

  // ngl this function is a bit of a mess.
  async postMessageToStore(body: {
    user_pguid: number;
    store_pguid: number;
    message: string;
  }): Promise<IGenericMessage | number> {
    let storeMessages;
    let messageSetIndex;
    let userName;

    const fetchStoreMessages = async () =>
      this._dbConnectorService
        .getStoreVault()
        .find({ is_private: false, public_guid: body.store_pguid })
        .project({
          _id: 0,
          store_messages: 1,
          store_title: 1,
        })
        .toArray();

    storeMessages = await fetchStoreMessages().then(
      (docs) => docs[0].store_messages
    );
    messageSetIndex = storeMessages.findIndex(
      (messageSet) => messageSet.source_guid === body.user_pguid
    );
    userName = await this._dbConnectorService
      .getUserVault()
      .find({ public_guid: body.user_pguid })
      .project({
        _id: 0,
        username: 1,
      })
      .toArray()
      .then((docs) => docs[0].username);

    if (messageSetIndex === -1) {
      const message = {
        source_guid: body.user_pguid,
        messages: [{ username: userName, message: body.message }],
      };
      storeMessages.push(message);
    } else {
      storeMessages[messageSetIndex].messages.push({
        username: userName,
        message: body.message,
      });
    }

    await this._dbConnectorService.getStoreVault().updateOne(
      { public_guid: body.store_pguid },
      {
        $set: {
          store_messages: storeMessages,
        },
      }
    );

    storeMessages = await fetchStoreMessages().then(
      (docs) => docs[0].store_messages
    );
    messageSetIndex = storeMessages.findIndex(
      (messageSet) => messageSet.source_guid === body.user_pguid
    );

    await this._dbConnectorService.closeConnection();

    if (messageSetIndex === -1) {
      return 501;
    } else {
      return storeMessages[messageSetIndex];
    }
  }

  async findAllStores(
    limit = environment.ALL_STORES_LIMIT
  ): Promise<Array<IStoreInterface>> {
    const allStores = await this._dbConnectorService
      .getStoreVault()
      .find({ is_private: false })
      .limit(limit)
      .project({
        _id: 0,
        own_guid: 0,
        parent_guid: 0,
        store_messages: 0,
      })
      .toArray();

    await this._dbConnectorService.closeConnection();

    return allStores;
  }

  async findSingleStore(
    storePGUID,
    limit = environment.SINGLE_STORE_LIMIT
  ): Promise<Array<IStoreInterface>> {
    const query = {
      is_private: false,
      public_guid: storePGUID,
    };

    const singleStore = await this._dbConnectorService
      .getStoreVault()
      .find(query)
      .limit(limit)
      .project({
        _id: 0,
        own_guid: 0,
        parent_guid: 0,
        store_messages: 0,
      })
      .toArray();

    await this._dbConnectorService.closeConnection();

    return singleStore;
  }

  async getStoreMessages(body: { user_pguid: number; store_pguid: number }) {
    const messages = await this._dbConnectorService
      .getStoreVault()
      .find({ is_private: false, public_guid: body.store_pguid })
      .project({
        _id: 0,
        store_messages: 1,
      })
      .toArray()
      .then((docs) => docs[0].store_messages);

    const messageSetIndex = messages.findIndex(
      (messageSet) => messageSet.source_guid === body.user_pguid
    );

    await this._dbConnectorService.closeConnection();

    if (messages !== -1) {
      return messages[messageSetIndex];
    }
    return 404;
  }
}
