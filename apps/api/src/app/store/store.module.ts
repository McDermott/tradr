import { Module } from '@nestjs/common';
import { StoreController } from './store.controller';
import { DbModule } from '../db/db.module';
import { StoreService } from './store.service';
import { environment } from '../../environments/environment';

@Module({
  imports: [DbModule.forRoot(environment.DATABASE_URL)],
  controllers: [StoreController],
  providers: [StoreService],
})
export class StoreModule {}
