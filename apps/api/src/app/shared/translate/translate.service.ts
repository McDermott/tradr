import { Injectable } from '@nestjs/common';

const en_GB = require('./../../../i18n/en-gb.json');

@Injectable()
export class TranslateService {
  constructor() {}
  public translate(stringAccessor) {
    const getPropValue = (obj, key) =>
      key.split('.').reduce((o, x) => (o == undefined ? o : o[x]), obj);
    return getPropValue(en_GB, stringAccessor);
  }
}
