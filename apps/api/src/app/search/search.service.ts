import { Injectable } from '@nestjs/common';
import {
  IBasicSearch,
  ISearchableItems,
} from '../../../../../libs/api-interfaces/src/lib/search.interface';
import { DbConnectorService } from '../db/db-connector.service';
import { IStoreInterface } from '../../../../../libs/api-interfaces/src/lib/user.interface';
import { searchType } from '../../../../../libs/api-interfaces/src/lib/enums';
import { environment } from '../../environments/environment';

@Injectable()
export class SearchService {
  constructor(private readonly _dbConnectorService: DbConnectorService) {}

  async getResults(
    basicSearch: IBasicSearch,
    limit = environment.SEARCH_RESULTS_LIMIT
  ): Promise<Array<IStoreInterface>> {
    const query = {
      is_private: false,
    };

    for (const [key, value] of Object.entries(basicSearch)) {
      if (key === searchType.STRING) {
        query[searchType.STRING] = {
          $regex: value,
        };
      }
      if (key === searchType.TAGS) {
        query[searchType.TAGS] = {
          $in: value,
        };
      }
      if (key === searchType.LOCATION) {
        query[searchType.TAGS] = {
          $in: value,
        };
      }
    }

    const results = await this._dbConnectorService
      .getStoreVault()
      .find(query)
      .limit(limit)
      .project({
        _id: 0,
        own_guid: 0,
        parent_guid: 0,
        store_messages: 0,
        can_message: 0,
        store_reviews: 0,
        store_banner: 0,
      })
      .toArray();

    await this._dbConnectorService.closeConnection();

    return results;
  }

  async getAllSearchables(): Promise<ISearchableItems> {
    const query = {
      is_private: false,
    };

    const allTagsWithCount = await this._dbConnectorService
      .getStoreVault()
      .find(query)
      .project({
        _id: 0,
        store_tags: 1,
      })
      .toArray()
      .then((tagSetBlock: Array<{ store_tags: Array<string> }>) => {
        const extractedTags = [];
        const completeTagSetWithQualifiers = {};

        tagSetBlock.forEach((tagSet) => {
          tagSet.store_tags.forEach((tag) => extractedTags.push(tag));
        });

        extractedTags.forEach((extractedTag) => {
          if (!completeTagSetWithQualifiers[extractedTag]) {
            completeTagSetWithQualifiers[extractedTag] = 1;
          } else {
            completeTagSetWithQualifiers[extractedTag]++;
          }
        });
        return completeTagSetWithQualifiers;
      });

    const allLocationsWithCount = await this._dbConnectorService
      .getStoreVault()
      .find(query)
      .project({
        _id: 0,
        store_location: 1,
      })
      .toArray()
      .then((tagSetBlock: Array<{ store_location: Array<string> }>) => {
        const extractedLocations = [];
        const completeLocationSetWithQualifiers = {};

        tagSetBlock.forEach((tagSet) => {
          extractedLocations.push(tagSet.store_location);
        });

        extractedLocations.forEach((extractedTag) => {
          if (!completeLocationSetWithQualifiers[extractedTag]) {
            completeLocationSetWithQualifiers[extractedTag] = 1;
          } else {
            completeLocationSetWithQualifiers[extractedTag]++;
          }
        });

        return completeLocationSetWithQualifiers;
      });

    await this._dbConnectorService.closeConnection();

    return {
      store_locations: allLocationsWithCount,
      store_tags: allTagsWithCount,
    };
  }
}
