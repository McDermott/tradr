import { Module } from '@nestjs/common';
import { SearchService } from './search.service';
import { SearchController } from './search.controller';
import { DbModule } from '../db/db.module';
import { environment } from '../../environments/environment';

@Module({
  imports: [DbModule.forRoot(environment.DATABASE_URL)],
  providers: [SearchService],
  controllers: [SearchController],
})
export class SearchModule {}
