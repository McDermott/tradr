import { Controller, Post, Request, Get } from '@nestjs/common';
import { SearchService } from './search.service';
import { IStoreInterface } from '../../../../../libs/api-interfaces/src/lib/user.interface';
import { ISearchableItems } from '../../../../../libs/api-interfaces/src/lib/search.interface';

@Controller()
export class SearchController {
  constructor(private readonly _searchService: SearchService) {}

  @Post('search')
  async search(@Request() req): Promise<Array<IStoreInterface>> {
    return await this._searchService.getResults(req.body);
  }

  @Get('lookup-searchables')
  async getSearchables(): Promise<ISearchableItems> {
    return await this._searchService.getAllSearchables();
  }
}
