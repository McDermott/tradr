import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { RegistrationService } from './registration.service';
import { DbModule } from '../db/db.module';
import { SharedModule } from '../shared/shared.module';
import { environment } from '../../environments/environment';

@Module({
  imports: [DbModule.forRoot(environment.DATABASE_URL), SharedModule],
  providers: [UsersService, RegistrationService],
  exports: [UsersService, RegistrationService],
})
export class UsersModule {}
