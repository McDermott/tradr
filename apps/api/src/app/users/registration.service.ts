import { Injectable } from '@nestjs/common';
import { DbConnectorService } from '../db/db-connector.service';
import {
  IStoreInterface,
  IUserInterface,
} from '../../../../../libs/api-interfaces/src/lib/user.interface';
import { uuidv4 } from '../../../../../libs/api-interfaces/src/lib/utils';
import { TranslateService } from '../shared/translate/translate.service';
import { DocType } from '../../../../../libs/api-interfaces/src/lib/enums';

const bcrypt = require('bcrypt');
const Filter = require('bad-words');

@Injectable()
export class RegistrationService {
  constructor(
    private readonly _dbConnectorService: DbConnectorService,
    private readonly _translateService: TranslateService
  ) {}

  public async registerUser(
    userData: IUserInterface
  ): Promise<number | { status: number; messages: string[] }> {
    const filter = new Filter();

    const errorMessageSet = {
      status: 409,
      messages: [],
    };

    const userExists = await this.validateIfUserExists(
      userData.username,
      userData.email
    );

    const storeExists = await this.validateIfStoreExists(
      (userData?.store_data_guid as IStoreInterface)?.store_title
    );

    const userNameIsProfane = filter.isProfane(userData.username);

    const storeTitleIsProfane = filter.isProfane(
      (userData?.store_data_guid as IStoreInterface)?.store_title
    );

    const storeDescIsProfane = filter.isProfane(
      (userData?.store_data_guid as IStoreInterface)?.store_description
    );

    if (userExists) {
      errorMessageSet.messages.push(
        this._translateService.translate('web.user_registration.user_exists')
      );
    }

    if (storeExists) {
      errorMessageSet.messages.push(
        this._translateService.translate('web.user_registration.store_exists')
      );
    }

    if (userNameIsProfane) {
      errorMessageSet.messages.push(
        this._translateService.translate(
          'web.user_registration.username_profane'
        )
      );
    }

    if (storeTitleIsProfane) {
      errorMessageSet.messages.push(
        this._translateService.translate(
          'web.user_registration.store_title_profane'
        )
      );
    }

    if (storeDescIsProfane) {
      errorMessageSet.messages.push(
        this._translateService.translate(
          'web.user_registration.store_desc_profane'
        )
      );
    }

    if (
      userData.store_data_guid &&
      !(userData?.store_data_guid as IStoreInterface)?.store_location
    ) {
      errorMessageSet.messages.push(
        this._translateService.translate(
          'web.user_registration.store_loc_required'
        )
      );
    }

    if (errorMessageSet.messages.length > 0) {
      return errorMessageSet;
    }

    const userAndStoreBuiltAndInserted = await this.buildUserAndStore(userData);

    if (userAndStoreBuiltAndInserted) {
      return {
        status: 200,
        messages: [
          this._translateService.translate(
            'web.user_registration.user_registered'
          ),
        ],
      };
    }

    return 500;
  }

  private async buildUserAndStore(user: IUserInterface): Promise<boolean> {
    // generate GUIDs for use across the user, store, address, etc.
    const userGUID = uuidv4();
    const publicUserGUID = uuidv4();
    const addressGUID = uuidv4();
    const storeGUID = uuidv4();
    const publicStoreGUID = uuidv4();

    const userData: IUserInterface = user;

    // Encrypt the user's password using bCrypt.
    userData.password = await bcrypt.hash(userData.password, 10);

    // Reassign the storeData to the GUID to point there for future reference.
    userData.base_guid = userGUID;
    userData.public_guid = publicUserGUID;
    userData.address_data.parent_guid = userGUID;
    userData.address_data.own_guid = addressGUID;
    userData.document_type = DocType.USER;

    if (!userData.user_avatar) {
      userData.user_avatar = undefined;
    }

    if (!userData.user_tags) {
      userData.user_tags = undefined;
    }

    let storeData;

    if (userData.store_data_guid) {
      storeData = userData.store_data_guid as IStoreInterface;

      userData.store_data_guid = storeGUID;

      storeData.parent_guid = userGUID;
      storeData.own_guid = storeGUID;
      storeData.public_guid = publicStoreGUID;
      storeData.endorsements = { number_of_endorsements: 0, endorsed_by: [] };
      storeData.store_reviews = [];
      storeData.store_messages = [];
      storeData.store_location = storeData.store_location.toLowerCase();
      storeData.document_type = DocType.STORE;
      storeData.store_search_string =
        storeData.store_title + ' ' + storeData.store_description;

      if (!storeData.store_avatar) {
        storeData.store_avatar = undefined;
      }

      if (!storeData.store_tags) {
        storeData.store_tags = undefined;
      }

      if (!storeData.store_banner) {
        storeData.store_banner = undefined;
      }
    } else {
      userData.store_data_guid = undefined;
    }

    const userInserted = !!(await this._dbConnectorService
      .getUserVault()
      .insertOne(userData));

    let storeInserted;

    if (storeData) {
      storeInserted = !!(await this._dbConnectorService
        .getStoreVault()
        .insertOne(storeData));
    }

    await this._dbConnectorService.closeConnection();

    return userInserted || (userInserted && storeInserted);
  }

  private async validateIfUserExists(
    username: string,
    email: string
  ): Promise<boolean> {
    const userNameQuery = await this._dbConnectorService
      .getUserVault()
      .findOne({ username });
    const emailQuery = await this._dbConnectorService
      .getUserVault()
      .findOne({ email });

    await this._dbConnectorService.closeConnection();

    return !!(userNameQuery || emailQuery);
  }

  private async validateIfStoreExists(storeTitle: string): Promise<boolean> {
    const userNameQuery = await this._dbConnectorService
      .getStoreVault()
      .findOne({
        store_title: storeTitle,
      });
    await this._dbConnectorService.closeConnection();
    return !!userNameQuery;
  }
}
