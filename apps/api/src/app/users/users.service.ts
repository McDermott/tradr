import { Injectable } from '@nestjs/common';
import { DbConnectorService } from '../db/db-connector.service';
import { IUserInterface } from '../../../../../libs/api-interfaces/src/lib/user.interface';

@Injectable()
export class UsersService {
  constructor(private readonly _dbConnectorService: DbConnectorService) {}

  async findOne(username: string): Promise<IUserInterface | null> {
    const singleUser = (await this._dbConnectorService
      .getUserVault()
      .findOne({ username })) as unknown as IUserInterface;
    await this._dbConnectorService.closeConnection();
    return singleUser;
  }
}
