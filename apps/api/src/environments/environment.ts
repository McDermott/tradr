export const environment = {
  production: false,
  DATABASE_URL: 'mongodb://localhost:27017/?retryWrites=true&w=majority',
  ALL_STORES_LIMIT: 100,
  SINGLE_STORE_LIMIT: 100,
  SEARCH_RESULTS_LIMIT: 100,
  SESSION_EXPIRATION: '600s',
  JWT_SECRET: 'e7ac0804-3a0f-47f7-a95c-12b3a6896d3b',
};
