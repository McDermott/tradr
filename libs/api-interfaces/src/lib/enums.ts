export enum DocType {
  USER = 'user',
  STORE = 'store',
  PRODUCT = 'product',
}

export enum searchType {
  STRING = 'store_search_string',
  LOCATION = 'store_tags',
  TAGS = 'store_location',
}

export enum storeThemes {
  DEFAULT = 'default', // White
  DARK = 'dark', // Dark #37393A
  SANDALWOOD = 'sandalwood', // Sandy brown #DBA159
  EARTH = 'earth', // Darker browns #322214
  MEADOW = 'meadow', // Light green #8DE969
  FOREST = 'forest', // Dark Green #1C5253
  SKY = 'sky', // Light blue #3581B8
  OCEAN = 'ocean', // Dark blue #0D2149
  CUPCAKE = 'cupcake', // Light red/Pink #E0607E
  SCARLET = 'scarlet', // Dark reds #322214
}

export enum craftTags {
  METAL_WORKING = 'metal_working',
  WOOD_WORKING = 'wood_working',
  PAPER_WORKING = 'paper_working',
  JEWELRY = 'jewelry',
  STONE_WORKING = 'stone_working',
  FOODSTUFFS = 'foodstuffs',
  KNITTING = 'knitting',
  NEEDLE_WORKING = 'needle_working',
  SEWING = 'sewing',
  TAILORING = 'tailoring',
  PAINTING = 'painting',
  DRAWING = 'drawing',
  SCULPTING = 'sculpting',
  CLAY_WORKING = 'clay_working',
  FLORAL_WORKING = 'floral_working',
  GARDENING = 'gardening',
  LIGHT_WORKING = 'light_working',
  LEATHER_WORKING = 'leather_working',
  HERBAL_WORKING = 'herbal_working',
  TRADITIONAL_SUPPLEMENTS = 'traditional_supplements',
  JOINERY = 'joinery',
  INLAY = 'inlay',
  MARKETRY = 'marketry',
  POTTERY = 'pottery',
  WEAVING = 'weaving',
  CROCHET = 'crochet',
  EMBROIDERY = 'embroidery',
  WOOD_CARVING = 'wood_carving',
  CALLIGRAPHY = 'calligraphy',
  BOOKBINDING = 'bookbinding',
  QUILTING = 'quilting',
  ORIGAMI = 'origami',
  DOLL_MAKING = 'doll_making',
  BEAD_WORK = 'bead_work',
  MACRAME = 'macrame',
  CANDLE_MAKING = 'candle_making',
  FELTING = 'felting',
  TABLETOP = 'tabletop',
  RELIGIOUS = 'religious',
  SEASONAL = 'seasonal',
}
