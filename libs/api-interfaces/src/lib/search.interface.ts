export interface IBasicSearch {
  search_string?: string;
  search_tags?: Array<string>;
  search_location?: Array<string>;
}

export interface ISearchableItems {
  store_locations: {
    [key: string]: number;
  };
  store_tags: {
    [key: string]: number;
  };
}
