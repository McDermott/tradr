import { craftTags, DocType, storeThemes } from './enums';

export interface IUserInterface {
  document_type?: DocType;
  public_guid?: string;
  base_guid?: string;
  username: string;
  password: string;
  first_name: string;
  last_name: string;
  email: string;
  user_avatar?: Blob | string;
  user_tags?: Array<craftTags>;
  address_data?: IAddressInterface;
  store_data_guid?: IStoreInterface | string; // This should be set by the user on POST, but then set as the store GUUID upon DB insertion
}

export interface IAddressInterface {
  parent_guid?: string; // Belongs to the User
  own_guid?: string; // Belongs to self.
  address_line_1?: string;
  address_line_2?: string;
  postcode?: string;
  county?: string;
  country?: string;
}

export interface IStoreInterface {
  document_type?: DocType;
  parent_guid?: string; // Belongs to the user.
  own_guid?: string; // Belongs to self.
  public_guid?: string;
  store_title?: string;
  store_description?: string;
  store_search_string?: string;
  store_avatar?: Blob | string;
  store_banner?: Blob | string;
  store_location?: string;
  can_message?: boolean;
  is_private?: boolean;
  endorsements?: {
    number_of_endorsements: number;
    endorsed_by?: Array<string>;
  };
  store_tags?: Array<craftTags>;
  store_theme?: storeThemes;
  store_items?: Array<IStoreProductInterface>;
  store_messages?: Array<IGenericMessage>;
  store_reviews?: Array<IGenericMessage>;
}

export interface IGenericMessage {
  source_guid?: string;
  messages: Array<{
    username: string;
    message: string;
  }>;
}

export interface IStoreProductInterface {
  document_type?: DocType;
  parent_guid?: string; // Belongs to the Store.
  own_guid?: string; // Belongs to self.
  product_name?: string;
  product_description?: string;
  is_private?: boolean;
  price?: number; // @TODO this will probably need to be more complex than this down the line.
  images?: Array<IStoreProductImageInterface>;
  comments?: Array<string>;
}

export interface IStoreProductImageInterface {
  parent_guid?: string; // Belongs to the Product
  own_guid?: string; // Belongs to self.
  image_name?: string;
  image_description?: string;
  image_data: Blob | string;
}

export interface ILoggedInUser {
  email?: string;
  first_name?: string;
  last_name?: string;
  public_guid?: string;
  store_data_guid?: string;
  username?: string;
}
